import numpy as np
from random import sample
from database import Database
from text_processing import TextProcessing
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import tensorflow as tf
import pickle

class DataLoader():
    def __init__(self):
        self.finished = False

    def load_manual(self, articles, titles):
        self.article_content = articles
        self.article_title = titles

    def load_automatic(self):
        article_title = Database().get_article_title()
        article_content = Database().get_article_content()
        self.article_content = article_content
        self.article_title = article_title

    def save_data(self, path):
        with open(path+"_content_encode", "wb") as f:
            pickle.dump(self.content, f)

        with open(path+"_title_encode", "wb") as f:
            pickle.dump(self.titles, f)

    def load_from_save(self, path):
        with open(path+"_content_encode", "rb") as f:
            self.content = pickle.load(f)
        with open(path+"_title_encode", "rb") as f:
            self.titles = pickle.load(f)

    def preprocess(self, dataset, apply_start_end_tag):
        data = []
        preprocesser = self.preprocesser
        for article in dataset:
            pa = preprocesser.clean_text(article[0])
            pa = preprocesser.contractions(pa)
            pa = preprocesser.normalize_text(pa)
            pa_tokens = preprocesser.tokenize_text(pa)
            #pa_tokens = preprocesser.remove_stopwords(pa_tokens)
            if(apply_start_end_tag):
                pa_tokens.insert(0, "<start>")
                pa_tokens.insert(len(pa_tokens), "<end>")
            #pa_tokens = preprocesser.tokenize_with_dict(pa_tokens, self.max_length)
            data.append(pa_tokens)
        return data

    def finish_processing(self, data, max_len, content = True):
        converted = self.vocab.convert_to_tokens(data, content)
        for c in converted: 
            if(len(c)> max_len):
                combined_data = []
                ml = int(max_len/2)
                combined_data += c[:ml]
                combined_data += c[-ml:]
                c = combined_data
        converted = pad_sequences(converted, maxlen=max_len, padding='post', truncating='post')
        converted = tf.cast(converted, dtype=tf.int32)
        return converted

    def load_and_preprocess(self, vocab, encoder_max_len, decoder_max_len, load_type = 0, save = False, path = "datasets/dataset",  vocab_path = "vocabularies/", save_vocab = False, articles = None, titles = None, train_vocab = False ):
        self.vocab = vocab
        self.preprocesser = TextProcessing(vocab)
        self.finished = False
        if load_type != 2:
            if load_type == 0:
                self.load_automatic()
            else:
                self.load_manual()
            self.content = self.preprocess(self.article_content, False)
            self.titles = self.preprocess(self.article_title, True)
            if train_vocab:
                self.vocab.create_vocab(self.content, self.titles)
                if save_vocab:
                    self.vocab.save_vocab(vocab_path)
            self.content = self.finish_processing(self.content, encoder_max_len, True)
            self.titles = self.finish_processing(self.titles, decoder_max_len, False)
        else:
            self.load_from_save(path)
        if save:
            self.save_data(path)
        self.finished = True

    def get_merged_sentices_articles(self):
        if self.finished:
            merged = []
            for article in self.content:
                for sentence in article:
                    merged.append(sentence)
            return merged
        return []

    def get_random_sentences_of_articles(self, num):
        if self.finished:
            articles = []
            for article in self.content:
                if len(article) >= num:
                    selected = sample(article, 5)
                    articles.append(selected)
            return articles
        return []

    def get_articles(self):
        if self.finished:
            return self.content
        return []

    def get_titles(self):
        if self.finished:
            return self.titles
        return []

class Vocabulary():
    article_vocab = None
    title_vocab = None

    def create_vocab(self, articles, titles):
        oov_token = '<unk>'
        article_tokenizer = Tokenizer(oov_token=oov_token)
        title_tokenizer = Tokenizer(oov_token=oov_token, filters = '!"#$%&()*+,-./:;=?@[\\]^_`{|}~\t\n')
        article_tokenizer.fit_on_texts(articles)
        title_tokenizer.fit_on_texts(titles)
        self.article_vocab = article_tokenizer
        self.title_vocab = title_tokenizer

    def convert_to_tokens(self, text, content=True):
        if self.article_vocab is not None and self.title_vocab is not None:
            if content:
                converted = self.article_vocab.texts_to_sequences(text)
            else:
                converted = self.title_vocab.texts_to_sequences(text)
            return converted
        return False

    def convert_to_token(self, word, content = False):
        if self.article_vocab is not None and self.title_vocab is not None:
            if content:
                converted = self.article_vocab.word_index[word]
            else:
                converted = self.title_vocab.word_index[word]

            return converted
        return False

    def convert_to_text(self, tokens, content=False):
        if self.article_vocab is not None and self.title_vocab is not None:
            if content:
                converted = self.article_vocab.sequences_to_texts(tokens)
            else:
                converted = self.title_vocab.sequences_to_texts(tokens)
            return converted
        return False

    def save_vocab(self, path):
        with open(path+'_articles.pickle', 'wb') as handle:
            pickle.dump(self.article_vocab, handle, protocol=pickle.HIGHEST_PROTOCOL)
        with open(path+'_titles.pickle', 'wb') as handle:
            pickle.dump(self.title_vocab, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def load_vocab(self, path):
        with open(path+'_articles.pickle', 'rb') as handle:
            self.article_vocab = pickle.load(handle)
        with open(path+'_titles.pickle', 'rb') as handle:
            self.title_vocab = pickle.load(handle)

    def is_empty(self):
        if self.article_vocab is None or self.title_vocab is None:
            return True
        return False

    def get_lengths(self):
        article_vocab_size = len(self.article_vocab.word_index) + 1
        title_vocab_size = len(self.title_vocab.word_index) + 1
        return article_vocab_size, title_vocab_size
from configparser import ConfigParser
import psycopg2

config = ConfigParser()
config.read("./config.ini")

class Database(object):
    def __init__(self) -> None:
        self.client = psycopg2.connect(host=config["database"]["host"], database=config["database"]["name"], user=config["database"]["user"], password=config["database"]["password"])
        self.cursor = self.client.cursor()

    def reset_generated_articles(self):
        self.cursor.execute("DROP TABLE paas_convertedarticle")
        self.client.close()

    def upsert_converted(self, url, tags, title, generated):
        tags = str(tags).replace('[', '{').replace(']', '}').replace("'", "\"")
        title = title.replace("'", "\"")
        
        self.cursor.execute("INSERT INTO paas_convertedarticle (url, tags, title, generated) VALUES (\'"+url+"\',\'"+tags+"\',\'"+title+"\',\'"+generated+"\') ON CONFLICT (url) DO UPDATE SET tags = excluded.tags, generated = excluded.generated")
        self.client.commit()

    def get_article_tags(self):
        self.cursor.execute("SELECT tags from article")
        record = self.cursor.fetchall()
        self.client.close()
        return record

    def get_article_urls(self):
        self.cursor.execute("SELECT url from article")
        record = self.cursor.fetchall()
        self.client.close()
        return record

    def get_article_content(self):
        self.cursor.execute("SELECT content from article")
        record = self.cursor.fetchall()
        self.client.close()
        return record

    def get_article_title(self):
        self.cursor.execute("SELECT title from article")
        record = self.cursor.fetchall()
        self.client.close()
        return record

    def get_generated_title(self):
        self.cursor.execute("SELECT generated from paas_convertedarticle order by id ASC")
        record = self.cursor.fetchall()
        self.client.close()
        return record

    def get_title_original(self):
        self.cursor.execute("SELECT title from paas_convertedarticle order by id ASC")
        record = self.cursor.fetchall()
        self.client.close()
        return record

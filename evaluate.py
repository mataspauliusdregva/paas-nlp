import enum
from database import Database
from text_processing import TextProcessing
from rouge_score import rouge_scorer
from absl import logging

import sklearn.metrics
import tensorflow as tf
from matplotlib import pyplot as plt
import numpy as np
import nltk.translate.bleu_score as bleu
import nltk.translate.meteor_score as meteor
import nltk
'''nltk.download('omw-1.4')'''

class Evaluate():
    def __init__(self) -> None:
        self.original_titles = [titles[0] for titles in Database().get_title_original()]
        self.generated_titles = [titles[0] for titles in Database().get_generated_title()]

        self.original_titles_tokens = [TextProcessing("").tokenize_text(titles[0]) for titles in Database().get_title_original()]
        self.generated_titles_tokens = [TextProcessing("").tokenize_text(titles[0]) for titles in Database().get_generated_title()]
        self.scorer = rouge_scorer.RougeScorer(['rouge1', 'rougeL'], use_stemmer=True)

    def calculate_bleu_meteor_scores(self):
        avg_bleu_score = 0
        avg_meteor_score = 0

        for count, sentence in enumerate(self.original_titles_tokens):
            score_bleu = bleu.sentence_bleu(sentence, self.generated_titles_tokens[count], weights=(1,0,0,0))
            score_meteor = meteor.meteor_score([[self.original_titles[count]]], [self.generated_titles[count]])
            avg_bleu_score = avg_bleu_score + score_bleu
            avg_meteor_score = avg_meteor_score + score_meteor

        avg_bleu_score = avg_bleu_score / np.shape(self.original_titles_tokens)[0]
        avg_meteor_score = avg_meteor_score / np.shape(self.original_titles_tokens)[0]
        print("1-gram Bleu score: {} Meteor score: {}".format(avg_bleu_score, avg_meteor_score))

    def calculate_rogue_score(self):
        avg_rouge_1_precision_score = 0
        avg_rouge_1_recall_score = 0
        avg_rouge_1_fmeasure_score = 0

        avg_rouge_L_precision_score = 0
        avg_rouge_L_recall_score = 0
        avg_rouge_L_fmeasure_score = 0

        for count, sentence in enumerate(self.original_titles):
            score_rogue = self.scorer.score(sentence, self.generated_titles[count])

            avg_rouge_1_precision_score = avg_rouge_1_precision_score + score_rogue['rouge1'][0]
            avg_rouge_1_recall_score = avg_rouge_1_recall_score + score_rogue['rouge1'][1]
            avg_rouge_1_fmeasure_score = avg_rouge_1_fmeasure_score + score_rogue['rouge1'][2]
            
            avg_rouge_L_precision_score = avg_rouge_L_precision_score + score_rogue['rougeL'][0]
            avg_rouge_L_recall_score = avg_rouge_L_recall_score + score_rogue['rougeL'][1]
            avg_rouge_L_fmeasure_score = avg_rouge_L_fmeasure_score + score_rogue['rougeL'][2]

        avg_rouge_1_precision_score = avg_rouge_1_precision_score / np.shape(self.original_titles)[0]
        avg_rouge_1_recall_score = avg_rouge_1_recall_score / np.shape(self.original_titles)[0]
        avg_rouge_1_fmeasure_score = avg_rouge_1_fmeasure_score / np.shape(self.original_titles)[0]

        avg_rouge_L_precision_score = avg_rouge_L_precision_score / np.shape(self.original_titles)[0]
        avg_rouge_L_recall_score = avg_rouge_L_recall_score / np.shape(self.original_titles)[0]
        avg_rouge_L_fmeasure_score = avg_rouge_L_fmeasure_score / np.shape(self.original_titles)[0]

        print("Rogue 1 precision score: {}\nRogue 1 recall score: {}\nRogue 1 f-measure score: {}\nRogue L precision score: {}\nRogue L recall score: {}\nRogue L f-measure score: {}\n".format(avg_rouge_1_precision_score, avg_rouge_1_recall_score, avg_rouge_1_fmeasure_score, avg_rouge_L_precision_score, avg_rouge_L_recall_score, avg_rouge_L_fmeasure_score))




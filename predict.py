import tensorflow as tf
import numpy as np
from database import Database
from transformer import Transformer, CustomSchedule
from helper import loss_function
from text_processing import TextProcessing

class Predict():
    def __init__(self, vocab, num_layers, d_model, num_heads, dff, article_vocab_size, title_vocab_size, checkpoint_path, encoder_maxlen, decoder_maxlen) -> None:
        learning_rate = CustomSchedule(d_model)
        self.encoder_maxlen = encoder_maxlen
        self.decoder_maxlen = decoder_maxlen
        self.vocab = vocab
        optimizer = tf.keras.optimizers.Adam(learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
        model = Transformer(num_layers, d_model, num_heads, dff, article_vocab_size, title_vocab_size, pe_input=article_vocab_size, pe_target=title_vocab_size)
        model.compile(optimizer=optimizer,loss=loss_function)
        model.load_weights(checkpoint_path)
        self.model = model

    def process_data(self, encoder_size, seq):
        preprocesser = TextProcessing("")
        pa = preprocesser.clean_text(seq)
        pa = preprocesser.contractions(pa)
        pa = preprocesser.normalize_text(pa)
        pa_tokens = preprocesser.tokenize_text(pa)

        text = self.vocab.convert_to_tokens([pa_tokens])
        text = tf.keras.preprocessing.sequence.pad_sequences(text, maxlen=encoder_size, padding='post', truncating='post')
        self.encoder_input = tf.expand_dims(text[0], 0)

        decoder_input = [self.vocab.convert_to_token("<start>")]
        self.decoder_input = tf.expand_dims(decoder_input, 0)

    def create_masks(self, result):
        encoder_padding_mask, combined_mask, decoder_padding_mask = self.model.create_masks(self.encoder_input, result)
        return encoder_padding_mask, combined_mask, decoder_padding_mask

    def predict_results(self, result, encoder_padding_mask, combined_mask, decoder_padding_mask):
        predictions, attention_weights = self.model.call(inp = self.encoder_input, tar = result, training=False, enc_padding_mask = encoder_padding_mask, dec_padding_mask = decoder_padding_mask, look_ahead_mask = combined_mask)
        return predictions, attention_weights


    def get_best_word_prediction(self, predictions):
        predictions = predictions[: ,-1:, :]
        predicted_id = tf.cast(tf.argmax(predictions, axis=-1), tf.int32)
        return predicted_id

    def process_predicted_data(self, seq):
        self.process_data(self.encoder_maxlen, seq)
        result = self.decoder_input
        #pradedant nuo <start> yra predictinamos reikšmės
        #kiekviena reikšmė yra įrašoma į masyvą
        #toliau iš  turimo masyvvo yra predictinama sekanti
        #taip kol pasiekiamas <end>
        for i in range(self.decoder_maxlen):
            encoder_padding_mask, combined_mask, decoder_padding_mask = self.create_masks(result)
            predictions, attention_weights = self.predict_results(result, encoder_padding_mask, combined_mask, decoder_padding_mask)
            predicted_word_id = self.get_best_word_prediction(predictions)
            
            if predicted_word_id == self.vocab.convert_to_token("<end>"):
                return tf.squeeze(result, axis=0), attention_weights

            result = tf.concat([result, predicted_word_id], axis=-1)
            self.weights = attention_weights
        return tf.squeeze(result, axis=0), attention_weights

    def show_results(self, seq):
        summarized = self.process_predicted_data(seq)[0].numpy()

        #praleidžiame start taga, end tag nebus nes loop sustoja ties juo
        summarized = np.expand_dims(summarized[1:], 0)

        #praleidžiame gautus kodavimus per žodyną, jog būtų atgauti žodžiai iš skaičių
        return self.vocab.convert_to_text(summarized)[0]

    def upsert_database(self):
        articles = Database().get_article_content()
        urls = Database().get_article_urls()
        titles = Database().get_article_title()
        tags = Database().get_article_tags()
        for count, article in enumerate(articles):
            res = self.show_results(article[0])
            Database().upsert_converted(url=urls[count][0], tags=tags[count][0], title=titles[count][0], generated=res)
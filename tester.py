import os
import tensorflow as tf

os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
os.environ['TF_XLA_FLAGS'] = '--tf_xla_enable_xla_devices'

gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
  tf.config.experimental.set_memory_growth(gpu, True)

from data_loader import DataLoader, Vocabulary
from evaluate import Evaluate
from transformer import Transformer, CustomSchedule
from helper import loss_function
from configparser import ConfigParser

evaluate = Evaluate()
evaluate.calculate_rogue_score()


'''titles = Database().get_article_title()
articles = Database().get_article_content()
data = []
preprocesser = TextProcessing("")
for article in articles:
    pa = preprocesser.clean_text(article[0])
    pa = preprocesser.contractions(pa)
    pa = preprocesser.normalize_text(pa)
    pa_tokens = preprocesser.tokenize_text(pa)
    data.append(pa_tokens)
     
articles_lengths = pd.Series([len(x) for x in data])

data = []
preprocesser = TextProcessing("")
for article in titles:
    pa = preprocesser.clean_text(article[0])
    pa = preprocesser.contractions(pa)
    pa = preprocesser.normalize_text(pa)
    pa_tokens = preprocesser.tokenize_text(pa)
    data.append(pa_tokens)


title_lengths = pd.Series([len(x) for x in data])
articles_lengths.describe()
title_lengths.describe()'''

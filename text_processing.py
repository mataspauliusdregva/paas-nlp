from lib2to3.pgen2 import token
import re
import nltk
import warnings
import numpy as np
import random
import string
import contractions

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer


'''
nltk.download('stopwords')
nltk.download('words')
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('wordnet')'''


class TextProcessing():
    def __init__(self, vocab) -> None:
        warnings.filterwarnings('ignore')
        self.stemmer = PorterStemmer()
        self.lemmatizer = WordNetLemmatizer()
        self.vocab = vocab

    def contractions(self, text):
        expanded_words = []   
        for word in text.split():
            expanded_words.append(contractions.fix(word))  
        expanded_text = ' '.join(expanded_words)
        expanded_text = re.sub(r'\'', '', expanded_text)
        return expanded_text

    def clean_text(self, text):

        # URL panaikinimas
        text = re.sub(r"http\S+", "", text)
        
        # Email panaikinimas
        text = re.sub(r'[\w\.-]+@[\w\.-]+', ' ', text, flags=re.MULTILINE)

        return text

    def normalize_text(self, text):

        #text = re.sub(r"\\\'", "", text)

        # Vertimas mažosiomis
        text = text.lower()
    
        # Kablelių panaikinimas
        '''text = re.sub(r'\d+', '', text)'''
        text = text.translate(str.maketrans("","", (string.punctuation + "—")))
        text = text.strip()

        return text

    def tokenize_text(self, text):
        tokens = nltk.word_tokenize(text)
        tokens = [x for x in tokens if len(x)>0]
        return tokens

    def remove_stopwords(self, tokens):
        
        #Stabdos žodžių panaikinimas
        stop_words = stopwords.words('english')
        words = [word for word in tokens if word not in stop_words]
        return words

    def split_article(self, tokens, max_length):
        x = [tokens[i:i + max_length] for i in range(0, len(tokens), max_length)] 
        return x

    def remove_sentences(self, tokens, max_length):
        x = [tokens[i] for i in range(len(tokens)) if len(tokens[i]) >= max_length]
        return x
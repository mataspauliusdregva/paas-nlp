import tensorflow as tf
from helper import *

class MultiHeadAttention(tf.keras.layers.Layer):
    def __init__(self, d_model, num_heads):
        super(MultiHeadAttention, self).__init__()
        self.num_heads = num_heads
        self.d_model = d_model

        #patikriname ar liekana 0, jei ne atsiranda klaida
        assert d_model % self.num_heads == 0

        #pasiskačiuojame gyli, dalyba ir suapvalinimas i 0 11/2 bus 5
        self.depth = d_model // self.num_heads

        #prisiskiriame svorius dense sluoksniamas
        self.wq = tf.keras.layers.Dense(d_model)
        self.wk = tf.keras.layers.Dense(d_model)
        self.wv = tf.keras.layers.Dense(d_model)

        self.dense = tf.keras.layers.Dense(d_model)
        
    def split_heads(self, x, batch_size):
        #keičiame dimensijas x pagal turimą batcha, imdami paskutinę reiksmę, gilį bei head sk.
        x = tf.reshape(x, (batch_size, -1, self.num_heads, self.depth))
        #transponuojame axis keiciame, default 0 1 2 3, gauname 0 2 1 3
        return tf.transpose(x, perm=[0, 2, 1, 3])
    
    def call(self, v, k, q, mask):
        batch_size = tf.shape(q)[0]

        q = self.wq(q)
        k = self.wk(k)
        v = self.wv(v)

        q = self.split_heads(q, batch_size)
        k = self.split_heads(k, batch_size)
        v = self.split_heads(v, batch_size)

        #paskaičiuojame kiek dėmesio kiekvienas žodis turi įgauti, yra svarbus
        scaled_attention, attention_weights = scaled_dot_product_attention(
            q, k, v, mask)

        #transponuojame į batch shape
        scaled_attention = tf.transpose(scaled_attention, perm=[0, 2, 1, 3])

        #reshapiname į q dimensijas, kokios buvo paduotos prieš skaičiuojant scaled attention
        concat_attention = tf.reshape(scaled_attention, (batch_size, -1, self.d_model))
        output = self.dense(concat_attention)
            
        return output, attention_weights

class EncoderLayer(tf.keras.layers.Layer):
    def __init__(self, d_model, num_heads, dff, rate=0.1):
        super(EncoderLayer, self).__init__()

        self.mha = MultiHeadAttention(d_model, num_heads)

        #2x dense sluoksniai
        self.ffn = point_wise_feed_forward_network(d_model, dff)

        self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)

        #neuronų atjungimo sluoksniai
        self.dropout1 = tf.keras.layers.Dropout(rate)
        self.dropout2 = tf.keras.layers.Dropout(rate)
    
    def call(self, x, training, mask):

        #multihead attention layer
        attn_output, _ = self.mha(x, x, x, mask)

        #dropout training, jog treniravimo metu nenaudosime dropout, neuronu atmetimo
        attn_output = self.dropout1(attn_output, training=training)

        #uždedame paduotam x attention weights, kad atmest nereikalingą informaciją, žodžius
        out1 = self.layernorm1(x + attn_output)

        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        out2 = self.layernorm2(out1 + ffn_output)

        return out2

class DecoderLayer(tf.keras.layers.Layer):
    def __init__(self, d_model, num_heads, dff, rate=0.1):
        super(DecoderLayer, self).__init__()

        #2x multihead sluoksniai
        self.mha1 = MultiHeadAttention(d_model, num_heads)
        self.mha2 = MultiHeadAttention(d_model, num_heads)

        #2x dense sluoksniai
        self.ffn = point_wise_feed_forward_network(d_model, dff)

        self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)

        self.dropout1 = tf.keras.layers.Dropout(rate)
        self.dropout2 = tf.keras.layers.Dropout(rate)
        self.dropout3 = tf.keras.layers.Dropout(rate)
    
    
    def call(self, x, enc_output, training, look_ahead_mask, padding_mask):
        attn1, attn_weights_block1 = self.mha1(x, x, x, look_ahead_mask)
        attn1 = self.dropout1(attn1, training=training)
        out1 = self.layernorm1(attn1 + x)

        attn2, attn_weights_block2 = self.mha2(enc_output, enc_output, out1, padding_mask)
        attn2 = self.dropout2(attn2, training=training)
        out2 = self.layernorm2(attn2 + out1)

        ffn_output = self.ffn(out2)
        ffn_output = self.dropout3(ffn_output, training=training)
        out3 = self.layernorm3(ffn_output + out2)

        return out3, attn_weights_block1, attn_weights_block2

class Encoder(tf.keras.layers.Layer):
    def __init__(self, num_layers, d_model, num_heads, dff, input_vocab_size, maximum_position_encoding, rate=0.1):
        super(Encoder, self).__init__()

        self.d_model = d_model
        self.num_layers = num_layers

        #sukuriame žodžių reprezentacijoms sukurti skirtas sluoksnis
        self.embedding = tf.keras.layers.Embedding(input_vocab_size, d_model)

        #metodas skirtas kiekvieno žodžio pozicijai įdėti į cos/sin funkcijas
        #cos/sin, jog kiekvienas žodis turėtų savo aiškią poziciją, nesutampančią
        #būtų žodžių tvarka, taip pat sin ir cos suteikia simetrijos, modeliui lengviau mokytis

        self.pos_encoding = positional_encoding(maximum_position_encoding, self.d_model)

        self.enc_layers = [EncoderLayer(d_model, num_heads, dff, rate) for _ in range(num_layers)]

        self.dropout = tf.keras.layers.Dropout(rate)
        
    def call(self, x, training, mask):
        seq_len = tf.shape(x)[1]

        x = self.embedding(x)

        #daugyba reikalinga, jog embedding sluoksnius turėtų daugiau įtakos nei pozicijos
        #treniravimo metu, tai yra žodžių vektoriai
        x *= tf.math.sqrt(tf.cast(self.d_model, tf.float32))
        x += self.pos_encoding[:, :seq_len, :]

        x = self.dropout(x, training=training)
    
        for i in range(self.num_layers):
            x = self.enc_layers[i](x, training, mask)
    
        return x

class Decoder(tf.keras.layers.Layer):
    def __init__(self, num_layers, d_model, num_heads, dff, target_vocab_size, maximum_position_encoding, rate=0.1):
        super(Decoder, self).__init__()

        self.d_model = d_model
        self.num_layers = num_layers

        self.embedding = tf.keras.layers.Embedding(target_vocab_size, d_model)
        self.pos_encoding = positional_encoding(maximum_position_encoding, d_model)

        self.dec_layers = [DecoderLayer(d_model, num_heads, dff, rate) for _ in range(num_layers)]
        self.dropout = tf.keras.layers.Dropout(rate)
    
    def call(self, x, enc_output, training, look_ahead_mask, padding_mask):
        seq_len = tf.shape(x)[1]
        attention_weights = {}

        x = self.embedding(x)
        x *= tf.math.sqrt(tf.cast(self.d_model, tf.float32))
        x += self.pos_encoding[:, :seq_len, :]

        x = self.dropout(x, training=training)

        for i in range(self.num_layers):
            x, block1, block2 = self.dec_layers[i](x, enc_output, training, look_ahead_mask, padding_mask)

            attention_weights['decoder_layer{}_block1'.format(i+1)] = block1
            attention_weights['decoder_layer{}_block2'.format(i+1)] = block2
    
        return x, attention_weights

class Transformer(tf.keras.Model):
    train_loss = tf.keras.metrics.Mean(name='train_loss')
    train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='train_accuracy')

    def __init__(self, num_layers, d_model, num_heads, dff, input_vocab_size, target_vocab_size, pe_input, pe_target, rate=0.1):
        super(Transformer, self).__init__()
        self.encoder = Encoder(num_layers, d_model, num_heads, dff, input_vocab_size, pe_input, rate)
        self.decoder = Decoder(num_layers, d_model, num_heads, dff, target_vocab_size, pe_target, rate)
        self.final_layer = tf.keras.layers.Dense(target_vocab_size)
    
    #sukuriame matrica
    def create_padding_mask(self,seq):
        seq = tf.cast(tf.math.equal(seq, 0), tf.float32)
        return seq[:, tf.newaxis, tf.newaxis, :]

    #virsutiniame matricos trikampyje bus 1111 nurodantys mask, tai yra 111 reikš, kurie žodžiai
    #atitinkamo žodžio atžvilgiu negali būti naudojami attentiono skaičiavimui, nes yra einantys po jo
    def create_look_ahead_mask(self,size):
        mask = 1 - tf.linalg.band_part(tf.ones((size, size)), -1, 0)
        return mask

    def create_masks(self, inp, tar):
        enc_padding_mask = self.create_padding_mask(inp)
        dec_padding_mask = self.create_padding_mask(inp)

        look_ahead_mask = self.create_look_ahead_mask(tf.shape(tar)[1])
        dec_target_padding_mask = self.create_padding_mask(tar)

        #ant sukurtos matricos uždedamas look_ahead_mask, tai yra paimamos max reikšmės atitinkamose pozicijose
        combined_mask = tf.maximum(dec_target_padding_mask, look_ahead_mask)
        return enc_padding_mask, combined_mask, dec_padding_mask

    def call(self, inp, training, **kwargs):
        tar = kwargs["tar"]
        enc_padding_mask = kwargs["enc_padding_mask"]
        look_ahead_mask = kwargs["look_ahead_mask"]
        dec_padding_mask = kwargs["dec_padding_mask"]
        enc_output = self.encoder(inp, training, enc_padding_mask)
        dec_output, attention_weights = self.decoder(tar, enc_output, training, look_ahead_mask, dec_padding_mask)
        final_output = self.final_layer(dec_output)
        return final_output, attention_weights

    def train_step(self, data):
        inp, tar = data

        #be <end> tag
        tar_inp = tar[:, :-1]
        #be <start> tag
        tar_real = tar[:, 1:]

        #sukuriamos matricos ir maskai
        enc_padding_mask, combined_mask, dec_padding_mask = self.create_masks(inp, tar_inp)

        with tf.GradientTape() as tape:
            predictions, _ = self(
                inp, 
                True, 
                tar = tar_inp ,
                enc_padding_mask = enc_padding_mask, 
                look_ahead_mask = combined_mask, 
                dec_padding_mask = dec_padding_mask
            )
            #lyginamas gautas su realiu rezultatu
            loss = self.compiled_loss(tar_real, predictions)
        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars) 
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))   
        self.train_loss.update_state(loss)
        self.train_accuracy(tar_real, predictions)
        return {m.name: m.result() for m in self.metrics}
    
    @property
    def metrics(self):
        return [self.train_loss] # self.train_accuracy

#klasė skirta learning rate keitimui treniravimo metu
#jei loss labai didelis ir auga learning rate mažinti ir pns
class CustomSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(self, d_model, warmup_steps=4000):
        super(CustomSchedule, self).__init__()

        self.d_model = d_model
        self.d_model = tf.cast(self.d_model, tf.float32)

        self.warmup_steps = warmup_steps
    
    def __call__(self, step):
        arg1 = tf.math.rsqrt(step)
        arg2 = step * (self.warmup_steps ** -1.5)

        return tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)